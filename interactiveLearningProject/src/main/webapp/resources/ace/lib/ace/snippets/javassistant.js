define(function(require, exports, module) {
"use strict";

exports.snippetText = require("../requirejs/text!./javassistant.snippets");
exports.scope = "javassistant";

});
