'use strict';

App.factory('LessonService', ['$http', '$q', function ($http, $q) {

    return {
        fetchFirstProjectStep: function (trackId, lessonIndex, stepIndex) {
            return $http.get('http://localhost:8080/rest/' + trackId + '/' + lessonIndex + '/PROJECT/' + stepIndex)
                .then(
                    function (response) {
                        console.log(response.data);
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Items');
                        return $q.reject(errResponse);
                    }
                );
        },
        fetchFirstInfoStep: function (trackId, lessonIndex) {
            return $http.get('http://localhost:8080/rest/' + trackId + '/' + lessonIndex + '/INFO')
                .then(
                    function (response) {
                        console.log(response.data);
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Items');
                        return $q.reject(errResponse);
                    }
                );
        },

        fetchTestLesson: function (trackId, lessonIndex) {
            return $http.get('http://localhost:8080/rest/' + trackId + '/' + lessonIndex + '/TEST')
                .then(
                    function (response) {
                        console.log(response.data);
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Items');
                        return $q.reject(errResponse);
                    }
                );
        },


        getProjectCode: function (path) {
            return $http.get('http://localhost:8080/rest/PROJECT/' + encodeURIComponent(path)) //encodeURIComponent(path)
                .then(
                    function (response) {
                        console.log(response.data);
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Items');
                        return $q.reject(errResponse);
                    }
                );
        },

        getCatchedProjectCode: function (path, token) {
            return $http.get('http://localhost:8080/rest/PROJECT/' + encodeURIComponent(path) + '/' + token) //encodeURIComponent(path)
                .then(
                    function (response) {
                        console.log(response.data);
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Items');
                        return $q.reject(errResponse);
                    }
                );
        },
        postCatchedProjectCode: function (path, token, code) {
            return $http.post('http://localhost:8080/rest/PROJECT/' + encodeURIComponent(path) + '/' + token, code) //encodeURIComponent(path)
                .then(
                    function (response) {
                        console.log(response.data);
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Items');
                        return $q.reject(errResponse);
                    }
                );
        },
        postEvaluationTestLesson: function (trackId, lesson, path, token, code) {
            return $http.post('http://localhost:8080/rest/PROJECT/eval/' + trackId + "/" + lesson +
                "/" + encodeURIComponent(path) + '/' + token, code)
                .then(
                    function (response) {
                        console.log(response.data);
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Items');
                        return $q.reject(errResponse);
                    }
                );
        }
    };

}]);
