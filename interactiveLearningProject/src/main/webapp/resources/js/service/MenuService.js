'use strict';

App.factory('MenuService', ['$http', '$q', function ($http, $q) {

    return {

        fetchLanguages: function () {//Fetches list from server.
            return $http.get('http://localhost:8080/rest/languages')
                .then(
                    function (response) {
                        console.log(response.data);
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Items');
                        return $q.reject(errResponse);
                    }
                );
        },

        fetchTracks: function (languageId) {//Fetches language list from server.
            return $http.get('http://localhost:8080/rest/language/' + languageId + '/tracks')
                .then(
                    function (response) {
                        console.log(response.data);
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Items');
                        return $q.reject(errResponse);
                    }
                );
        }
    };

}]);
