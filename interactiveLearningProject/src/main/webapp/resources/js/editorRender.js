function drawPopUp() {
    $(document).ready(function () {

        let height = $(".highTextComment").height();
        console.log(height);

        var el = $(".highTextComment");
        let negativeIndex = -1;

        const rowHeight = 20;
        el.css('margin-top', negativeIndex * (height + rowHeight) + 'px');

    });
}

function notifyLessonPassed($scope, notify, lessIndex) {

    var messageTemplate = '<div class="notify-template-class"> <img src="../../resources/img/tick.png"></div> ' +
        '<p>Congrats!!!</p>' +
        '<p>You successful passed a lesson!</p>';

    $scope.positions = ['center', 'left', 'right'];
    $scope.position = $scope.positions[2];

    $scope.duration = 3000;

    $scope.demo = {
        displayNotify: function () {
            notify({
                messageTemplate: messageTemplate,
                classes: $scope.classes,
                templateUrl: $scope.template,
                position: $scope.position,
                duration: $scope.duration
            });
        }
    };
    if (lessIndex != 0) {
        $scope.demo.displayNotify();
    }
}




