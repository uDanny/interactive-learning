'use strict';

App.controller('ProjectLessonController', ['async', '$scope', 'LessonService', 'notify', function (async, $scope, LessonService, notify) {
    var self = this;
    self.ProjectLessonDTO = async;

    notifyLessonPassed($scope, notify, self.ProjectLessonDTO.lessonIndex);

    var langTools = ace.require("ace/ext/language_tools");
    var focusedText = self.ProjectLessonDTO.step.focusedFilePath;
    $scope.initialFocusedPath = focusedText.substring(focusedText.lastIndexOf('\\') + 1);
    $scope.focusMessage = undefined;
    $scope.focusedFile = $scope.initialFocusedPath;
    $scope.actualCodePath = focusedText.toString().replace(/\\/g, '$').replace('.', '&');
    $scope.actualStepLineIndex = 0;
    $scope.stepLineDisplay = true;
    $scope.stepDisplay = true;
    $scope.lessonDisplay = true;
    $scope.roleList = self.ProjectLessonDTO.step.folderElement;
    $scope.code = self.ProjectLessonDTO.initialCode;
    $scope.lessonService = LessonService;

    if (self.ProjectLessonDTO.stepCount > eval(self.ProjectLessonDTO.stepIndex + 1)) {
        $scope.stepDisplay = true;
    } else {
        $scope.stepDisplay = false;
    }

    if (self.ProjectLessonDTO.lessonCount > eval(self.ProjectLessonDTO.lessonIndex + 1)) {
        $scope.lessonDisplay = true;
    } else {
        $scope.lessonDisplay = false;
    }

    $scope.actualStepLineIndex = 0;

    $scope.stepLine = self.ProjectLessonDTO.step.stepLines[$scope.actualStepLineIndex];

    var stepLength = self.ProjectLessonDTO.step.stepLines.length;

    if (stepLength <= 1) {
        $scope.stepLineDisplay = false;
    }
    $scope.changeRoute = function (url, forceReload) {
        $scope = $scope || angular.element(document).scope();
        if (forceReload || $scope.$$phase) { // that's right TWO dollar signs: $$phase
            window.location = url;
        } else {
            $location.path(url);
            $scope.$apply();
        }
    };

    $scope.nextCustomStep = function () {
        if ($scope.stepLineDisplay) {
            if ($scope.actualStepLineIndex + 2 >= stepLength) {
                $scope.stepLineDisplay = false;
            }
            $scope.actualStepLineIndex++;
            $scope.stepLine = self.ProjectLessonDTO.step.stepLines[$scope.actualStepLineIndex];
            $scope.renderCustomElement.runRender();
        } else if ($scope.stepDisplay) {
            $scope.changeRoute('#/' + self.ProjectLessonDTO.trackId + '/' + self.ProjectLessonDTO.lessonIndex + '/PROJECT/' + eval(1 + self.ProjectLessonDTO.stepIndex));
        } else if ($scope.lessonDisplay) {
            $scope.changeRoute('#/' + self.ProjectLessonDTO.trackId + '/' + eval(1 + self.ProjectLessonDTO.lessonIndex) + '/' + self.ProjectLessonDTO.nextLessonType + '/0');
        }
    };

    $scope.codeCaller = {
        callCode: function (path) {
            LessonService.getProjectCode(path)
                .then(function (response) {
                    $scope.code = response.response;
                });
            if ($scope.focusedFile != $scope.initialFocusedPath && $scope.stepLineDisplay) {
                $scope.focusMessage = $scope.initialFocusedPath;
            } else {
                $scope.focusMessage = undefined;
            }
        }
    };

    $scope.renderCustomElement = {
        runRender: function applyCustomElements() {
            // alert('Clicked !');

            var topicIndex = $scope.actualStepLineIndex;

            var lines = angular.element(document.getElementsByClassName('ace_text-layer')).children();

            for (var i = 0; i < lines.length; i++) {

                var eachLine = angular.element(lines[i]);

                var lineChildren = angular.element(eachLine).children();
                var isModified = isModifiedLine();

                if (isModified) {
                    let highlights = getHighlightsTags();

                    if (highlights.aChangedLineArray != undefined && highlights.aChangedLineArray.length > 0) {
                        eachLine.addClass('newLine');
                    }

                    if (highlights.aHighLineArray != undefined && highlights.aHighLineArray.length > 0) {
                        for (let eachHighChild of highlights.aHighLineArray) {
                            let index = highlights.aHighLineArray.indexOf(eachHighChild);
                            let comment = angular.element(highlights.aHighLineCommentArray[index]).text().replace('/aHighComment', '').replace('aHighComment', '').trim();
                            var fullHighText = angular.element(eachHighChild).text();
                            fullHighText = fullHighText.replace('/aHighLine', '').replace('aHighLine', '').trim();
                            var {foundFullHighlightChildren, fullLineChildText} = isFoundChangedFullHighlightChildren(eachHighChild, comment);
                            if (!foundFullHighlightChildren) {
                                var fullLineHtml = angular.element(eachLine).html();
                                var eachElementHighLine = [];
                                getElementsToBeHighlighted(eachElementHighLine);
                                var toChange = searchCodeHighlightElements();
                                eachLine.html(addHighlightToFoundElements(fullLineHtml, toChange, comment));

                            }

                        }
                    }
                    if (angular.element(eachLine).hasClass('topicLine')) {
                        angular.element(eachLine).removeClass('topicLine');
                    }
                    if (highlights.aTopicLineArray != undefined && highlights.aTopicLineArray.length > 0) {
                        let step = angular.element(highlights.aTopicLineArray[0]).text().replace('/aTopicLine', '').replace('aTopicLine', '').trim();
                        if (step == topicIndex) {
                            eachLine.addClass('topicLine');
                        }
                    }
                }
                eachLine.addClass("eachLine");

            }

            function isModifiedLine() {
                var isModified = false;
                for (let lineChild of lineChildren) {
                    if (angular.element(lineChild).hasClass('ace_aLine')) {
                        isModified = true;
                        break;
                    }
                }
                return isModified;
            }

            function getElementsToBeHighlighted(eachElementHighLine) {
                const elementsRegex = /[a-zA-Z0-9]+|./g;
                let m;
                while ((m = elementsRegex.exec(fullHighText)) !== null) {
                    if (m.index === elementsRegex.lastIndex) {
                        elementsRegex.lastIndex++;
                    }

                    m.forEach((match, groupIndex) => {
                        eachElementHighLine.push(match);
                    });
                }
            }

            function searchCodeHighlightElements() {
                const textRegex = />[^<>]+</g;
                let toChange = {
                    start: [],
                    end: []
                }
                let m;
                let elementHighLineIndex = 0;
                var BreakException = {};

                try {
                    while ((m = textRegex.exec(fullLineHtml)) !== null) {
                        if (m.index === textRegex.lastIndex) {
                            textRegex.lastIndex++;
                        }
                        m.forEach((match, groupIndex) => {
                            let textElement = match.substring(1, match.length - 1);

                            if (textElement == eachElementHighLine[elementHighLineIndex]) {
                                toChange.start.push(m.index + 1);
                                toChange.end.push(m.index + 1 + textElement.length);
                                elementHighLineIndex++;
                                if (toChange.start.length == eachElementHighLine.length) {
                                    throw BreakException;
                                }
                            }
                        });
                    }
                } catch (e) {
                    if (e !== BreakException) throw e;
                }
                return toChange;
            }

            function addHighlightToFoundElements(fullLineHtml, toChange, comment) {
                var incrementalLength = 0;
                for (let i = 0; i < toChange.start.length; i++) {

                    let firstPart = "<span class=\"highText\">";
                    let secondPart = "<span class=\"highTextComment\">" + comment + "</span></span>";

                    fullLineHtml = spliceSplit(fullLineHtml, toChange.start[i] + incrementalLength, 0, "<span class=\"highText\">");
                    incrementalLength += firstPart.length;
                    fullLineHtml = spliceSplit(fullLineHtml, toChange.end[i] + incrementalLength, 0, secondPart);
                    incrementalLength += secondPart.length;

                }
                return fullLineHtml;
            }

            function spliceSplit(str, index, count, add) {
                var ar = str.split('');
                ar.splice(index, count, add);
                return ar.join('');
            }

            function getHighlightsTags() {
                var highlights = {
                    aChangedLineArray: [],
                    aHighLineArray: [],
                    aHighLineCommentArray: [],
                    aTopicLineArray: [],
                };


                for (let lineChild of lineChildren) {
                    if (angular.element(lineChild).hasClass('ace_aChangedLine')) {
                        highlights.aChangedLineArray.push(lineChild)
                    }
                    if (angular.element(lineChild).hasClass('ace_aHighLine')) {
                        highlights.aHighLineArray.push(lineChild)
                    }
                    if (angular.element(lineChild).hasClass('ace_aHighComment')) {
                        highlights.aHighLineCommentArray.push(lineChild)
                    }
                    if (angular.element(lineChild).hasClass('ace_aTopicLine')) {
                        highlights.aTopicLineArray.push(lineChild)
                    }
                }
                return highlights;
            }

            function isFoundChangedFullHighlightChildren(eachHighChild, comment) {
                var foundFullHighlightChildren = false;

                for (let lineChild of lineChildren) {
                    if (lineChild != eachHighChild) {
                        var fullLineChildText = angular.element(lineChild).text();
                        if (fullLineChildText == fullHighText) { //if full childs are the same
                            lineChild.innerHTML =
                                '<span class="highText">' + fullLineChildText +
                                '<span class="highTextComment">' + comment + ' </span>' +
                                '</span>';
                            foundFullHighlightChildren = true;
                            break;
                        }
                    }
                }
                return {foundFullHighlightChildren, fullLineChildText};
            }

            drawPopUp();
        }
    };

    $scope.load = function (_editor) {
        _editor.setOptions({
            enableBasicAutocompletion: true,
            enableSnippets: true,
            enableLiveAutocompletion: true,
        });
        _editor.setReadOnly(true);
    };


}
]);

App.directive('editorBind', [function () {
    return {
        template: '<div id="editorWindow" class="editorWindow" >' + '<div class="editor" ui-ace="{ mode: \'javassistant\', theme: \'xcode\',onLoad:load }" ng-model="code">' + '</div>',
        link: function (scope, elem, attrs) {
            bindAllElements(scope, elem);
        }
    }
}]);

function bindAllElements(scope, elem) {
    elem.ready(function () {
            var targetNode = document.getElementById('editorWindow');
            console.log(targetNode);
            var config = {childList: true, subtree: true};
            var callback = function (val) {
                console.log(val);
                observer.disconnect();
                scope.renderCustomElement.runRender();
                observer.observe(targetNode, config);
            };
            var observer = new MutationObserver(callback);
            observer.observe(targetNode, config);
        }
    )

}
