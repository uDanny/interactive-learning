'use strict';

App.controller('TeacherController', ['async', 'FileUploader', '$scope', function (async, FileUploader, $scope) {
    var self = this;
    self.languages = async;
    $scope.allLanguages = self.languages;
    $scope.selectedLang = $scope.allLanguages[0];
    $scope.projectName = '';
    $scope.response = '';

    $scope.uploader = new FileUploader();

    $scope.uploader.onCompleteItem = function (fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
        $scope.response = response.replace("<StringResponseDTO><response>", "").replace("</response></StringResponseDTO>", "");

    };

}]);
