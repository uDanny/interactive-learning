'use strict';

App.controller('TestLessonController', ['async', '$scope', 'LessonService', 'notify', function (async, $scope, LessonService, notify) {
    var self = this;
    self.TestLessonDTO = async;

    notifyLessonPassed($scope, notify, self.TestLessonDTO.lessonIndex);

    $scope.token = self.TestLessonDTO.token;
    $scope.exerciseText = self.TestLessonDTO.testLesson.exercise;
    $scope.resultText = undefined;

    var langTools = ace.require("ace/ext/language_tools");
    var focusedText = self.TestLessonDTO.testLesson.focusedFilePath;
    $scope.actualCodePath = focusedText.toString().replace(/\\/g, '$').replace('.', '&');
    $scope.initialFocusedPath = focusedText.substring(focusedText.lastIndexOf('\\') + 1);
    $scope.focusedFile = $scope.initialFocusedPath;
    $scope.codeResponseStyle = undefined;
    $scope.codeAccepted = undefined;

    $scope.lastCheckPath = $scope.actualCodePath;
    $scope.lessonDisplay = true;
    $scope.roleList = self.TestLessonDTO.testLesson.folderElement;
    $scope.code = self.TestLessonDTO.initialCode;
    $scope.lessonService = LessonService;

    if (self.TestLessonDTO.lessonCount > eval(self.TestLessonDTO.lessonIndex + 1)) {
        $scope.lessonDisplay = true;
    } else {
        $scope.lessonDisplay = false;
    }

    $scope.codeCaller = {
        callCode: function (path) {
            console.log(" codeCaller ");
            console.log($scope.actualCodePath);
            console.log($scope.lastCheckPath);
            LessonService.postCatchedProjectCode($scope.actualCodePath, $scope.token, $scope.code);

            LessonService.getCatchedProjectCode(path, $scope.token)
                .then(function (response) {
                    $scope.code = response.response;
                    $scope.lastCheckPath = path;
                });

        }
    };

    $scope.evalProject = function () {

        LessonService.postEvaluationTestLesson(self.TestLessonDTO.trackId, self.TestLessonDTO.lessonIndex, $scope.lastCheckPath, $scope.token, $scope.code)
            .then(function (response) {
                $scope.resultText = response.response;
                $scope.passedScore = response.accepted;

                if (response.accepted == true) {
                    $scope.codeResponseStyle = 'success-response';
                    $scope.codeAccepted = true;
                } else {
                    $scope.codeResponseStyle = 'error-response';
                    $scope.codeAccepted = false;
                }
            });
    };

    $scope.load = function (_editor) {
        _editor.setOptions({
            enableBasicAutocompletion: true,
            enableSnippets: true,
            enableLiveAutocompletion: true,
        });
        _editor.setReadOnly(false);
    };
}
])
;

App.directive('editorBinder', [function () {
    return {
        template: '<div id="editorWindow" class="editorWindow" >' + '<div class="editor" ui-ace="{ mode: \'javassistant\', theme: \'xcode\',onLoad:load }" ng-model="code">' + '</div>',
        link: function (scope, elem, attrs) {
        },
    }
}]);

