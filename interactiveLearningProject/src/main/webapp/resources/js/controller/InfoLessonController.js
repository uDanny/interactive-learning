'use strict';

App.controller('InfoLessonController', ['async', '$scope', 'LessonService', 'notify', function (async, $scope, LessonService, notify) {
    var self = this;
    self.InfoLessonDTO = async;

    notifyLessonPassed($scope, notify, self.InfoLessonDTO.lessonIndex);

    $scope.htmlText = self.InfoLessonDTO.htmlText;

    $scope.lessonDisplay = true;

    if (self.InfoLessonDTO.lessonCount > eval(self.InfoLessonDTO.lessonIndex + 1)) {
        $scope.lessonDisplay = true;
    } else {
        $scope.lessonDisplay = false;
    }

}
]);
App.directive('infoExplication', [function () {
    return {
        template: '<div id="infoWindow">' + '</div>',
        link: function (scope, elem, attrs) {
            elem.html(scope.htmlText);
        }
    }
}]);
