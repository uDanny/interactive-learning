'use strict';

var App = angular.module('myApp', ['ui.router', 'angularTreeview', 'ui.ace', 'angularFileUpload', 'cgNotify']);

App.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/language");

    $stateProvider
    //menu routing
        .state('language', {
            url: "/language",
            templateUrl: 'template/languages',
            controller: "LanguageController as langCtrl",
            resolve: {
                async: ['MenuService', function (MenuService) {
                    return MenuService.fetchLanguages();
                }]
            }

        })
        .state('language.track', {
            parent: 'language',
            url: '/{languageId:[0-9]{0,9}}',
            templateUrl: 'template/tracks',
            controller: "TrackController as trackCtrl",
            resolve: {
                async: ['MenuService', '$stateParams', function (MenuService, $stateParams) {
                    return MenuService.fetchTracks($stateParams.languageId);
                }]
            }
        })
        .state('teacher', {
            url: "/teacher",
            templateUrl: 'template/teacher',
            controller: "TeacherController as teachCtrl",
            resolve: {
                async: ['MenuService', function (MenuService) {
                    return MenuService.fetchLanguages();
                }]
            }

        })

        //lesson router
        .state('firstLesson', {
            url: '/{trackId:[0-9]{0,9}}/{lessonIndex:[0-9]{0,9}}/{nextLessonType:[A-Za-z]{0,9}}/{stepIndex:[0-9]{0,9}}',
            templateUrl: function ($stateParams) {

                if ($stateParams.nextLessonType == "PROJECT") {
                    return 'template/lesson/PROJECT';
                } else if ($stateParams.nextLessonType == "INFO") {
                    return 'template/lesson/INFO';
                }else if ($stateParams.nextLessonType == "TEST") {
                    return 'template/lesson/TEST';
                }
            },
            controllerProvider: function ($stateParams) {
                if ($stateParams.nextLessonType == "PROJECT") {
                    return "ProjectLessonController as projCtrl";
                } else if ($stateParams.nextLessonType == "INFO") {
                    return "InfoLessonController as infoCtrl";
                } else if ($stateParams.nextLessonType == "TEST") {
                    return "TestLessonController as testCtrl";
                }
            },
            resolve: {
                async: ['LessonService', '$stateParams', function (LessonService, $stateParams) {
                    if ($stateParams.nextLessonType == "PROJECT") {
                        return LessonService.fetchFirstProjectStep($stateParams.trackId, $stateParams.lessonIndex, $stateParams.stepIndex);
                    } else if ($stateParams.nextLessonType == "INFO") {
                        return LessonService.fetchFirstInfoStep($stateParams.trackId, $stateParams.lessonIndex);
                    } else if ($stateParams.nextLessonType == "TEST") {
                        return LessonService.fetchTestLesson($stateParams.trackId, $stateParams.lessonIndex);
                    }
                }]
            },
        })
        .state('achievement', {
            url: "/achievement",
            templateUrl: 'template/achieve',
            controller: "AchievementLessonController as achieveCtrl",
        })

}])
;
App.run(function ($rootScope, $window) {
    $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {
            if (toState.external) {
                event.preventDefault();
                $window.open(toState.url, '_self');
            }
        });
})