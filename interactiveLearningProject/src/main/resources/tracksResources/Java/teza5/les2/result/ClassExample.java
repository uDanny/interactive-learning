package result;

public class ClassExample {
    private String classStringProperty;

    public String getClassStringProperty() {
        return classStringProperty;
    }

    public void setClassStringProperty(String classStringProperty) {
        this.classStringProperty = classStringProperty;
    }
}
