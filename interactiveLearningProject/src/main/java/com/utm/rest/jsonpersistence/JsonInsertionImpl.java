package com.utm.rest.jsonpersistence;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utm.rest.dto.impl.InfoLessonDTO;
import com.utm.rest.dto.impl.ProjectLessonDTO;
import com.utm.rest.dto.impl.TestLessonDTO;
import com.utm.rest.enums.LessonType;
import com.utm.rest.model.Step;
import com.utm.rest.model.TestLesson;
import com.utm.rest.model.TrackContainer;
import org.hibernate.Session;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


@Repository("jsonInsertion")
public class JsonInsertionImpl implements JsonInsertion {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public void insertValueById(int id, TrackContainer trackContainer) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(trackContainer);

        Connection conn = getConnection();

        try {
            String sql = "INSERT INTO track_container VALUES (?, ?::JSON)";
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setInt(1, id);
            ps.setObject(2, jsonInString);
            ps.executeUpdate();

            conn.commit();

        } catch (
                Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public ProjectLessonDTO getStep(int trackId, int lessonIndex, int stepIndex) {

        Connection conn = getConnection();

        String sql = "SELECT\n" +
                "  (SELECT json_array_length(json_container -> 'lessons') FROM track_container WHERE id = ?) AS lesson_count,\n" +
                "  (SELECT json_array_length(json_container -> 'lessons'->?->'steps') FROM track_container WHERE id = ?) AS step_count,\n" +
                "  (json_container -> 'lessons'->?->'steps'->?) AS lesson,\n" +
                "  (json_container -> 'lessons' -> ? -> 'lessonType') AS next_lesson_type\n" +
                "FROM track_container WHERE id = ?";
        ProjectLessonDTO projectLessonDTO = null;

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, trackId);
            ps.setInt(2, lessonIndex);
            ps.setInt(3, trackId);
            ps.setInt(4, lessonIndex);
            ps.setInt(5, stepIndex);
            ps.setInt(6, lessonIndex + 1);
            ps.setInt(7, trackId);
            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                try {
                    ProjectLessonDTO foundProject = new ProjectLessonDTO();
                    foundProject.setLessonIndex(lessonIndex);
                    foundProject.setLessonCount(resultSet.getInt(1));
                    foundProject.setStepIndex(stepIndex);
                    foundProject.setStepCount(resultSet.getInt(2));
                    foundProject.setStep((new ObjectMapper()).readValue(resultSet.getString(3), Step.class));
                    foundProject.setNextLessonType(
                            resultSet.getString(4) != null ?
                                    LessonType.valueOf(getStringValuesFromJson(resultSet.getString(4).toUpperCase())) : null
                    );
                    projectLessonDTO = foundProject;
                } catch (JsonParseException e) {
                    e.printStackTrace();
                } catch (JsonMappingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return projectLessonDTO;
    }

    @Override
    public TestLessonDTO getTestLesson(int trackId, int lessonIndex) {

        Connection conn = getConnection();

        String sql = "SELECT\n" +
                "  (SELECT json_array_length(json_container -> 'lessons') FROM track_container WHERE id = ?) AS lesson_count,\n" +
                "  (json_container -> 'lessons' -> ? -> 'lessonType') AS next_lesson_type,\n" +
                "  (json_container -> 'lessons' -> ?)   AS lesson\n" +
                "FROM track_container\n" +
                "WHERE id = ?;";
        TestLessonDTO testLessonDTO = null;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, trackId);
            ps.setInt(2, lessonIndex + 1);
            ps.setInt(3, lessonIndex);
            ps.setInt(4, trackId);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                try {
                    TestLessonDTO foundLesson = new TestLessonDTO();
                    foundLesson.setLessonIndex(lessonIndex);
                    foundLesson.setLessonCount(resultSet.getInt(1));
                    foundLesson.setNextLessonType(
                            resultSet.getString(2) != null ?
                                    LessonType.valueOf(getStringValuesFromJson(resultSet.getString(4).toUpperCase())) : null
                    );
                    foundLesson.setTestLesson((new ObjectMapper()).readValue(resultSet.getString(3), TestLesson.class));
                    testLessonDTO = foundLesson;
                } catch (JsonParseException e) {
                    e.printStackTrace();
                } catch (JsonMappingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return testLessonDTO;
    }

    @Override
    public InfoLessonDTO getInfoLesson(int trackId, int lessonIndex) {

        Connection conn = getConnection();

        String sql = "SELECT\n" +
                "  (SELECT json_array_length(json_container -> 'lessons') FROM track_container WHERE id = ?) AS lesson_count,\n" +
                "  (json_container -> 'lessons'-> ? ->'htmlText') AS lesson,\n" +
                "  (json_container -> 'lessons' -> ? -> 'lessonType') AS next_lesson_type\n" +
                "FROM track_container WHERE id = ?;";
        InfoLessonDTO infoLessonDTO = null;

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, trackId);
            ps.setInt(2, lessonIndex);
            ps.setInt(3, lessonIndex + 1);
            ps.setInt(4, trackId);
            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                InfoLessonDTO foundInfo = new InfoLessonDTO();
                foundInfo.setLessonIndex(lessonIndex);
                foundInfo.setLessonCount(resultSet.getInt(1));
                foundInfo.setHtmlText(getStringValuesFromJson(resultSet.getString(2)));
                foundInfo.setNextLessonType(
                        resultSet.getString(3) != null ?
                                LessonType.valueOf(getStringValuesFromJson(resultSet.getString(3).toUpperCase())) : null
                );
                infoLessonDTO = foundInfo;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return infoLessonDTO;
    }

    private String getStringValuesFromJson(String resultString) {
        if (resultString != null) {
            return resultString.replaceAll("\"", "").trim();
        }
        return null;
    }

    private Connection getConnection() {
        Session session = (Session) entityManager.getDelegate();
        SessionFactoryImplementor sfi = (SessionFactoryImplementor) session.getSessionFactory();
        ConnectionProvider cp = sfi.getConnectionProvider();
        Connection conn = null;
        try {
            conn = cp.getConnection();
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }


}
