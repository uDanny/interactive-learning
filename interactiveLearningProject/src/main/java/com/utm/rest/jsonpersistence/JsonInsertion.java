package com.utm.rest.jsonpersistence;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.utm.rest.dto.impl.InfoLessonDTO;
import com.utm.rest.dto.impl.ProjectLessonDTO;
import com.utm.rest.dto.impl.TestLessonDTO;
import com.utm.rest.model.TrackContainer;
import org.json.simple.JSONObject;

public interface JsonInsertion {

    void insertValueById(int id, TrackContainer trackContainer) throws JsonProcessingException;

    ProjectLessonDTO getStep(int trackId, int lessonIndex, int stepIndex);

    TestLessonDTO getTestLesson(int trackId, int lessonIndex);

    InfoLessonDTO getInfoLesson(int trackId, int lessonIndex);
}
