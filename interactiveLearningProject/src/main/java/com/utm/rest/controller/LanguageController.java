package com.utm.rest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.utm.rest.model.Language;
import com.utm.rest.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LanguageController {

    @Autowired
    LanguageService languageService;

    @RequestMapping(value = "/rest/languages",  method = RequestMethod.GET)
    public ResponseEntity<List> listAllLanguages() throws JsonProcessingException {
        System.out.println("*************************************ListAllLanguages");
        List<Language> categories = languageService.getAll();
        if (categories.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }
}
