package com.utm.rest.controller;

import com.utm.rest.model.Track;
import com.utm.rest.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TrackController {

    @Autowired
    TrackService trackService;

    @RequestMapping(value = "/rest/language/{id}/tracks", method = RequestMethod.GET)
    public ResponseEntity<List> listAllTracksByLang(@PathVariable("id") int id) {
        System.out.println("*************************************ListTracksByLang");
        List<Track> categories = trackService.getAllByLanguage(id);
        if (categories.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        ResponseEntity<List> listResponseEntity = new ResponseEntity<>(categories, HttpStatus.OK);
        return listResponseEntity;
    }
}
