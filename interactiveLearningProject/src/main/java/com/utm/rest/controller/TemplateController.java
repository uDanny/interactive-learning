package com.utm.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TemplateController {

    @RequestMapping(value = "/template/languages")
    public String getLanguagesTemplate() {
        return "template/all_languages";
    }

    @RequestMapping(value = "/template/tracks")
    public String getTracksTemplate() {
        return "template/all_tracks";
    }

    @RequestMapping(value = "/template/lesson/PROJECT")
    public String getProjectLessonTemplate() {
        return "template/project_container";
    }

    @RequestMapping(value = "/template/lesson/INFO")
    public String getInfoLessonTemplate() {
        return "template/info_container";
    }

    @RequestMapping(value = "/template/lesson/TEST")
    public String getTestLessonTemplate() {
        return "template/test_container";
    }

    @RequestMapping(value = "/template/teacher")
    public String getTeacherTemplate() {
        return "template/teacher_page";
    }

    @RequestMapping(value = "/template/achieve")
    public String getAchieveTemplate() {
        return "template/achievement_container";
    }

}

