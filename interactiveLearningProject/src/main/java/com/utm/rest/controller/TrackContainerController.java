package com.utm.rest.controller;

import com.utm.rest.dto.impl.*;
import com.utm.rest.jsonpersistence.JsonInsertion;
import com.utm.rest.redis.CacheService;
import com.utm.rest.service.ProjectEvaluator;
import com.utm.rest.service.StorageService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Random;

@RestController
public class TrackContainerController {

    public static final int MAX_RANDOM_BOUND = 20000;
    @Autowired
    JsonInsertion jsonInsertion;

    @Autowired
    StorageService storageService;

    @Autowired
    CacheService cacheService;

    @Autowired
    ProjectEvaluator projectEvaluator;

    @RequestMapping(value = "/rest/{trackId}/{lessonIndex}/PROJECT/{stepIndex}", method = RequestMethod.GET)
    public ResponseEntity<ProjectLessonDTO> getStep(@PathVariable("trackId") int trackId, @PathVariable("lessonIndex") int lessonIndex, @PathVariable("stepIndex") int stepIndex) throws IOException {
        System.out.println("*************************************getStep");

        ProjectLessonDTO projectLessonDTO = jsonInsertion.getStep(trackId, lessonIndex, stepIndex);
        projectLessonDTO.setInitialCode(storageService.getStepInitialCode(projectLessonDTO.getStep().getFocusedFilePath()));
        projectLessonDTO.setTrackId(trackId);
        if (projectLessonDTO == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(projectLessonDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{trackId}/{lessonIndex}/TEST", method = RequestMethod.GET)
    public ResponseEntity<TestLessonDTO> getTestLesson(@PathVariable("trackId") int trackId, @PathVariable("lessonIndex") int lessonIndex) throws IOException {
        System.out.println("*************************************getTestLesson");

        TestLessonDTO testLesson = jsonInsertion.getTestLesson(trackId, lessonIndex);
        testLesson.setTrackId(trackId);
        testLesson.setInitialCode(storageService.getStepInitialCode(testLesson.getTestLesson().getFocusedFilePath()));
        testLesson.setToken(Math.abs(new Random().nextInt(MAX_RANDOM_BOUND)));
        if (testLesson == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(testLesson, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{trackId}/{lessonIndex}/INFO", method = RequestMethod.GET)
    public ResponseEntity<InfoLessonDTO> getInfoLesson(@PathVariable("trackId") int trackId, @PathVariable("lessonIndex") int lessonIndex) throws IOException {
        System.out.println("*************************************getInfoLesson");

        InfoLessonDTO infoLessonDTO = jsonInsertion.getInfoLesson(trackId, lessonIndex);
        infoLessonDTO.setTrackId(trackId);
        if (infoLessonDTO == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(infoLessonDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/PROJECT/{projectPath}", method = RequestMethod.GET)
    public ResponseEntity<StringResponseDTO> getProjectCode(@PathVariable("projectPath") String projectPath) throws IOException {
        System.out.println("*************************************getProjectCode");
        String resultCode = storageService.getProjectCode(replaceSymbols(projectPath));
        if (StringUtils.isBlank(resultCode)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(new StringResponseDTO(resultCode), HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/PROJECT/{projectPath}/{token}", method = RequestMethod.GET)
    public ResponseEntity<StringResponseDTO> getTokenProjectCode(@PathVariable("projectPath") String projectPath, @PathVariable("token") Integer token) throws IOException {
        System.out.println("*************************************getTokenProjectCode");

        String resultCode;
        List<String> strings = cacheService.listMessages(token + replaceSymbols(projectPath));

        if (CollectionUtils.isNotEmpty(strings) && StringUtils.isNotBlank(strings.get(0))) {
            resultCode = strings.get(0);
        } else {
            resultCode = storageService.getProjectCode(replaceSymbols(projectPath));
        }
        if (StringUtils.isBlank(resultCode)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(new StringResponseDTO(resultCode), HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/PROJECT/{projectPath}/{token}", method = RequestMethod.POST)
    public ResponseEntity<StringResponseDTO> postTokenProjectCode(@PathVariable("projectPath") String projectPath, @PathVariable("token") Integer token, @RequestBody String code) throws IOException {
        System.out.println("*************************************postTokenProjectCode");
        cacheService.addMessage(token + replaceSymbols(projectPath), code);
        return new ResponseEntity<>(new StringResponseDTO("Success"), HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/PROJECT/eval/{trackId}/{lessonIndex}/{projectPath}/{token}", method = RequestMethod.POST)
    public ResponseEntity<StatefulStringResponseDTO> postEvalProject(@PathVariable("trackId") int trackId, @PathVariable("lessonIndex") int lessonIndex, @PathVariable("projectPath") String projectPath, @PathVariable("token") Integer token, @RequestBody String code) throws IOException {
        System.out.println("*************************************postEvalProject");
        cacheService.addMessage(token + replaceSymbols(projectPath), code);
        TestLessonDTO testLesson = jsonInsertion.getTestLesson(trackId, lessonIndex);

        StatefulStringResponseDTO result = projectEvaluator.compileProject(testLesson, token);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    private String replaceSymbols(String projectPath) {
        return projectPath.replaceAll("\\$", "\\\\").replaceAll("&", ".");
    }
}
