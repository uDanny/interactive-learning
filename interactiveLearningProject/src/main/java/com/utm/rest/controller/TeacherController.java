package com.utm.rest.controller;

import com.utm.rest.dto.impl.StringResponseDTO;
import com.utm.rest.exceptions.TrackNameExistException;
import com.utm.rest.jsonpersistence.JsonInsertion;
import com.utm.rest.model.TrackContainer;
import com.utm.rest.service.LanguageService;
import com.utm.rest.service.ProjectImportService;
import com.utm.rest.service.StorageService;
import com.utm.rest.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class TeacherController {
    public static final String WRONG_LANGUAGE = "WrongLanguage";
    public static final String SUCCESS_UPLOAD = "Success upload";
    @Autowired
    StorageService storageService;

    @Autowired
    ProjectImportService projectImportService;

    @Autowired
    LanguageService languageService;

    @Autowired
    JsonInsertion jsonInsertion;

    @Autowired
    TrackService trackService;

    @RequestMapping(value = "/rest/upload/{languageId}/{trackName}", method = RequestMethod.POST)
    public ResponseEntity<StringResponseDTO> uploadProject(@PathVariable("languageId") int languageId, @PathVariable("trackName") String trackName, @RequestParam("file") MultipartFile file) throws IOException {
        System.out.println("*************************************uploadTrack");

        String lang = projectImportService.getValidLanguageName(languageId, trackName);
        if (lang == null) {
            return new ResponseEntity(new StringResponseDTO(WRONG_LANGUAGE), HttpStatus.NOT_ACCEPTABLE);
        }
        storageService.processZipFile(lang, trackName, file);
        TrackContainer trackContainer = projectImportService.scanProjectFiles(lang, trackName);

        projectImportService.persistTrack(trackContainer, languageId, trackName);
        return new ResponseEntity(new StringResponseDTO(SUCCESS_UPLOAD), HttpStatus.OK);

    }

    @ResponseBody
    @ExceptionHandler({TrackNameExistException.class})
    public ResponseEntity<StringResponseDTO> handleException(TrackNameExistException e) {
        return new ResponseEntity(new StringResponseDTO(e.getTrackName() + " - track name is already taken, please choose another one"), HttpStatus.OK);

    }
}
