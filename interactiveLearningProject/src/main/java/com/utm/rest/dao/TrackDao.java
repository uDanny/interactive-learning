package com.utm.rest.dao;

import com.utm.rest.model.Track;
import com.utm.rest.model.TrackContainer;

import java.util.List;

public interface TrackDao {
    List<Track> getAll();

    List<Track> getAllByLanguageID(int languageId);

    Integer persistGetId(Track track);
}
