package com.utm.rest.dao.impl;

import com.utm.rest.dao.LanguageDao;
import com.utm.rest.model.Language;
import com.utm.rest.model.Language_;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("languageDao")
public class LanguageDaoImpl extends AbstractDao<Integer, Language> implements LanguageDao {

    @Override
    public List<Language> getAll() {
        System.out.println("*************************************getAllLanguages");
        return retreiveAll();
    }

    @Override
    public Language getById(int languageId) {
        System.out.println("*************************************getLangById");
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Language> cq = cb.createQuery(Language.class);
        Root<Language> r = cq.from(Language.class);
        cq.where(cb.equal(r.get(Language_.id), languageId));
        Query query = entityManager.createQuery(cq);

        return (Language) query.getSingleResult();
    }
}
