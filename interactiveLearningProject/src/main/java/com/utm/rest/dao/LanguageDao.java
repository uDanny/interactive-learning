package com.utm.rest.dao;

import com.utm.rest.model.Language;

import java.util.List;

public interface LanguageDao {
    List<Language> getAll();

    Language getById(int langId);
}
