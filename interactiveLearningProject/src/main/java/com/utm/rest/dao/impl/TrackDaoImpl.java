package com.utm.rest.dao.impl;

import com.utm.rest.dao.TrackDao;
import com.utm.rest.model.Track;
import com.utm.rest.model.TrackContainer;
import com.utm.rest.model.Track_;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("trackDao")
public class TrackDaoImpl extends AbstractDao<Integer, Track> implements TrackDao {

    @Override
    public List<Track> getAll() {
        System.out.println("*************************************getAllTracks");
        return retreiveAll();
    }

    @Override
    public List<Track> getAllByLanguageID(int languageId) {
        System.out.println("*************************************getAllTracksByLanguageId");
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Track> cq = cb.createQuery(Track.class);
        Root<Track> r = cq.from(Track.class);
        cq.where(cb.equal(r.get(Track_.language_id), languageId));
        Query query = entityManager.createQuery(cq);

        List<Track> result = query.getResultList();


        return result;
    }

    @Override
    public Integer persistGetId(Track track) {
        entityManager.persist(track);
        entityManager.flush();
        return track.getId();
    }
}
