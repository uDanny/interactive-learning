package com.utm.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.utm.rest.model.Lesson;
import com.utm.rest.model.ProjectLesson;
import com.utm.rest.model.Step;
import com.utm.rest.model.TrackContainer;
import com.utm.rest.enums.LessonType;
import com.utm.rest.filecrawler.FileCrawlerImpl;
import com.utm.rest.filecrawler.model.FolderElement;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Client {

    public static final String focusedPath = "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\main\\resources\\tracksResources\\java\\helloworld2\\a";
    public static final String rootPath = "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\main\\resources\\tracksResources\\java\\helloworld2";

    public static TrackContainer main(String[] args) throws JsonProcessingException {

        String path = rootPath;

        Step step = new Step();
        //TODO: list fo folder elements
        FolderElement[] folderElement = new FileCrawlerImpl().getPath(path);
        FileCrawlerImpl.setFocused(folderElement, focusedPath);
        step.setFolderElement(folderElement);

        List<Step> steps = new ArrayList<>();
        steps.add(step);

        ProjectLesson projectLesson = new ProjectLesson();
        projectLesson.setLessonType(LessonType.PROJECT);
        projectLesson.setSteps(steps);

        List<Lesson> lessons = new LinkedList<>();
        lessons.add(projectLesson);
        lessons.add(null);

        return new TrackContainer(lessons);
    }
}
