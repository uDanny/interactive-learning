package com.utm.rest.exceptions;

public class TrackNameExistException extends RuntimeException {
    String trackName;

    public TrackNameExistException(String trackName) {
        super();
        this.trackName = trackName;
    }

    public String getTrackName() {
        return trackName;
    }
}
