package com.utm.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.utm.rest.filecrawler.model.FolderElement;

import java.util.ArrayList;
import java.util.List;

public class Step {

    @JsonProperty("stepLines")
    private List<StepLine> stepLines;

    @JsonProperty("focusedFilePath")
    private String focusedFilePath;

    @JsonProperty("focusedFileName")
    private String focusedFileName;

    @JsonProperty("folderElement")
    private FolderElement[] folderElement;

    public Step() {
        stepLines = new ArrayList<>();
    }

    public List<StepLine> getStepLines() {
        return stepLines;
    }

    public void setStepLines(List<StepLine> stepLines) {
        this.stepLines = stepLines;
    }

    public String getFocusedFilePath() {
        return focusedFilePath;
    }

    public void setFocusedFilePath(String focusedFilePath) {
        this.focusedFilePath = focusedFilePath;
    }

    public String getFocusedFileName() {
        return focusedFileName;
    }

    public void setFocusedFileName(String focusedFileName) {
        this.focusedFileName = focusedFileName;
    }

    public FolderElement[] getFolderElement() {
        return folderElement;
    }

    public void setFolderElement(FolderElement[] folderElement) {
        this.folderElement = folderElement;
    }

}
