package com.utm.rest.model;

import com.utm.rest.enums.LessonType;

import javax.persistence.*;

@Entity
@Table(name = "track")
public class Track {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "track_id")
    private int id;

    @Column(name = "language_id", nullable = false)
    private int language_id;

    @Column(name = "track_name", nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "first_lesson_type", nullable = false)
    private LessonType firstLessonType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(int language_id) {
        this.language_id = language_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LessonType getFirstLessonType() {
        return firstLessonType;
    }

    public void setFirstLessonType(LessonType firstLessonType) {
        this.firstLessonType = firstLessonType;
    }
}
