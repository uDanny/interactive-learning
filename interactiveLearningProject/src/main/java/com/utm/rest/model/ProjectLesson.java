package com.utm.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.utm.rest.enums.LessonType;

import java.util.ArrayList;
import java.util.List;

@JsonTypeName("projectLesson")
public class ProjectLesson extends Lesson {

    @JsonProperty("steps")
    private List<Step> steps;

    public ProjectLesson() {
        steps = new ArrayList<>();
        setLessonType(LessonType.PROJECT);
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }
}
