package com.utm.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.utm.rest.enums.LessonType;

import java.io.Serializable;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({

        @JsonSubTypes.Type(value = ProjectLesson.class, name = "projectLesson"),

        @JsonSubTypes.Type(value = InfoLesson.class, name = "infoLesson")

})
public abstract class Lesson implements Serializable {

    @JsonProperty("lessonIndex")
    private int index;

    @JsonProperty("lessonType")
    private LessonType lessonType;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public LessonType getLessonType() {
        return lessonType;
    }

    public void setLessonType(LessonType lessonType) {
        this.lessonType = lessonType;
    }
}
