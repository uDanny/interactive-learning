package com.utm.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StepLine {

    @JsonProperty("id")
    private int id;

    @JsonProperty("htmlText")
    private String htmlText;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }
}
