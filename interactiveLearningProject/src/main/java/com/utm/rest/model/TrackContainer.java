package com.utm.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.utm.rest.enums.LessonType;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "trackContainer")
public class TrackContainer {

    @JsonProperty("firstLessonType")
    private LessonType firstLessonType;

    @JsonProperty("lessons")
    private List<Lesson> lessons;

    public TrackContainer() {
        lessons = new ArrayList<>();
    }

    public TrackContainer(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public LessonType getFirstLessonType() {
        return firstLessonType;
    }

    public void setFirstLessonType(LessonType firstLessonType) {
        this.firstLessonType = firstLessonType;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

}
