package com.utm.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.utm.rest.enums.LessonType;

@JsonTypeName("infoLesson")
public class InfoLesson extends Lesson{
    @JsonProperty("htmlText")
    private String htmlText;

    public InfoLesson() {
        setLessonType(LessonType.INFO);
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }
}
