package com.utm.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.utm.rest.enums.LessonType;
import com.utm.rest.filecrawler.model.FolderElement;

import java.util.ArrayList;
import java.util.List;

@JsonTypeName("testLesson")
public class TestLesson extends Lesson {

    @JsonProperty("testFilePath")
    private String testFilePath;

    @JsonProperty("focusedFilePath")
    private String focusedFilePath;

    @JsonProperty("folderElement")
    private FolderElement[] folderElement;

    @JsonProperty("exercise")
    private String exercise;

    public TestLesson() {
        setLessonType(LessonType.TEST);
    }

    public String getFocusedFilePath() {
        return focusedFilePath;
    }

    public void setFocusedFilePath(String focusedFilePath) {
        this.focusedFilePath = focusedFilePath;
    }

    public FolderElement[] getFolderElement() {
        return folderElement;
    }

    public void setFolderElement(FolderElement[] folderElement) {
        this.folderElement = folderElement;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public String getTestFilePath() {
        return testFilePath;
    }

    public void setTestFilePath(String testFilePath) {
        this.testFilePath = testFilePath;
    }
}
