package com.utm.rest.redis;

import java.util.List;

public interface CacheService {
    void addMessage(String user, String message);

    List<String> listMessages(String user);
}
