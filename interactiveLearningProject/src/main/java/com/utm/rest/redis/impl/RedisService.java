package com.utm.rest.redis.impl;

import com.utm.rest.redis.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

@Service
public class RedisService implements CacheService {

    @Autowired
    private StringRedisTemplate template;


    @Override
    public void addMessage(String token, String message) {

        ListOperations<String, String> listOps = template.opsForList();
        listOps.leftPush(token, message);
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        Date date = Date.from(zonedDateTime.plus(2, ChronoUnit.HOURS).toInstant());
        template.expireAt(token, date);
    }

    @Override
    public List<String> listMessages(String token) {
        return template.opsForList().range(token, 0, -1);
    }
}
