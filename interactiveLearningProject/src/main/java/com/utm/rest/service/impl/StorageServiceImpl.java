package com.utm.rest.service.impl;

import com.utm.rest.service.StorageService;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service("storageService")
public class StorageServiceImpl implements StorageService {

    static final String trackDestination = "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\main\\resources\\tracksResources";
    static final String tempDestination = "D:";

    @Override
    public void processZipFile(String langFolder, String trackName, MultipartFile file) throws IOException {
        File zip = File.createTempFile(UUID.randomUUID().toString(), "temp", new File(tempDestination));
        try (FileOutputStream o = new FileOutputStream(zip)) {
            IOUtils.copy(file.getInputStream(), o);
        }

        try {
            ZipFile zipFile = new ZipFile(zip);
            zipFile.extractAll(trackDestination + "\\" + langFolder + "\\" + trackName);
        } catch (ZipException e) {
            e.printStackTrace();
        } finally {
            zip.delete();
        }
    }

    @Override
    public String getStepInitialCode(String filePath) throws IOException {
        if (StringUtils.isNotBlank(filePath)) {
            Path path = Paths.get(filePath);
            return new String(Files.readAllBytes(path));
        }
        return null;
    }

    @Override
    public String getProjectCode(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }


}
