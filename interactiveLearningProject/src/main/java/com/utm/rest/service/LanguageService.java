package com.utm.rest.service;

import com.utm.rest.model.Language;

import java.util.List;

public interface LanguageService {
    List<Language> getAll();

    Language getById(int langId);
}
