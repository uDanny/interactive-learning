package com.utm.rest.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface StorageService {
    void processZipFile(String langFolder, String trackName, MultipartFile file) throws IOException;

    String getStepInitialCode(String filePath) throws IOException;

    String getProjectCode(String path) throws IOException;
}
