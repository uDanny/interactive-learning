package com.utm.rest.service;

import com.utm.rest.model.Track;

import java.util.List;

public interface TrackService {
    List<Track> getAllByLanguage(int langugeId);

    Integer persistGetId(Track trackContainer);
}
