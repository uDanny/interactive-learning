package com.utm.rest.service.impl;

import com.utm.rest.dto.impl.StatefulStringResponseDTO;
import com.utm.rest.service.ShellService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;

@Service
public class ShellServiceImpl implements ShellService {


    public static final String JAVAC_PATTERN = "javac -cp \"{0};{1}\" {1}\\{2}.java\n";
    public static final String JAVA_PATTERN = "java -cp  \".;{0}\";{1} {2}";

    @Override
    public StatefulStringResponseDTO runWithJar(String junitJarPath, String testPath, String testFileName) throws Exception {
        StatefulStringResponseDTO responseDTO = new StatefulStringResponseDTO("execution code: ", true);
        runProcess(MessageFormat.format(JAVAC_PATTERN, junitJarPath, testPath, testFileName), responseDTO);
        concatString(responseDTO, "\nrun code  ");
        runProcess(MessageFormat.format(JAVA_PATTERN, junitJarPath, testPath, testFileName), responseDTO);

        return responseDTO;

    }

    private void runProcess(String command, StatefulStringResponseDTO responseDTO) throws Exception {
        Process pro = Runtime.getRuntime().exec(command);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(printLines(" \n out:", pro.getInputStream()));
        stringBuilder.append(printLines(" \n err:", pro.getErrorStream()));
        pro.waitFor();
        stringBuilder.append(" exitValue() " + pro.exitValue());
        concatString(responseDTO, stringBuilder.toString());
        setAcceptance(responseDTO, pro);

    }

    private StringBuilder printLines(String cmd, InputStream ins) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;
        BufferedReader in = new BufferedReader(
                new InputStreamReader(ins));
        while ((line = in.readLine()) != null) {
            stringBuilder.append(cmd + " " + line);
        }
        return stringBuilder;
    }

    private void setAcceptance(StatefulStringResponseDTO responseDTO, Process pro) {
        if (responseDTO.isAccepted()){
            if (pro.exitValue() == 0) {
                responseDTO.setAccepted(true);
            } else {
                responseDTO.setAccepted(false);
            }
        }
    }

    private void concatString(StatefulStringResponseDTO responseDTO, String newMessage) {
        if (StringUtils.isNotBlank(responseDTO.getResponse()) && StringUtils.isNotBlank(newMessage)) {
            responseDTO.setResponse(responseDTO.getResponse() + newMessage);
        }
    }

}
