package com.utm.rest.service.impl;

import com.utm.rest.dao.LanguageDao;
import com.utm.rest.model.Language;
import com.utm.rest.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("languageService")
@Transactional
public class LanguageServiceImpl implements LanguageService {

    @Autowired
    LanguageDao languageDao;

    @Override
    public List<Language> getAll() {
        List<Language> languages = languageDao.getAll();
        return languages;
    }

    @Override
    public Language getById(int langId) {
        return languageDao.getById(langId);
    }

}
