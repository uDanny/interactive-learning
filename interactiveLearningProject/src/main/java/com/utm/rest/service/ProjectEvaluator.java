package com.utm.rest.service;

import com.utm.rest.dto.impl.StatefulStringResponseDTO;
import com.utm.rest.dto.impl.TestLessonDTO;

import java.io.IOException;

public interface ProjectEvaluator {
    StatefulStringResponseDTO compileProject(TestLessonDTO testLesson, Integer token) throws IOException;
}
