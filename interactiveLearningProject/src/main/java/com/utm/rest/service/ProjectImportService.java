package com.utm.rest.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.utm.rest.model.TrackContainer;

import java.io.IOException;

public interface ProjectImportService {
    TrackContainer scanProjectFiles(String langPath, String containerPath) throws IOException;

    String getValidLanguageName(int langId, String trackName);

    void persistTrack(TrackContainer trackContainer, int languageId, String trackName) throws JsonProcessingException;
}
