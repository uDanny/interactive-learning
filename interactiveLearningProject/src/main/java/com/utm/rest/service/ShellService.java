package com.utm.rest.service;

import com.utm.rest.dto.impl.StatefulStringResponseDTO;

public interface ShellService {
    StatefulStringResponseDTO runWithJar(String junitJarPath, String testPath, String testFileName) throws Exception;
}
