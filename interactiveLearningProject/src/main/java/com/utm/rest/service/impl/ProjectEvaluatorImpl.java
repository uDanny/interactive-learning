package com.utm.rest.service.impl;

import com.utm.rest.dto.impl.StatefulStringResponseDTO;
import com.utm.rest.dto.impl.TestLessonDTO;
import com.utm.rest.redis.CacheService;
import com.utm.rest.service.ProjectEvaluator;
import com.utm.rest.service.ShellService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

@Service("projectEvaluator")
public class ProjectEvaluatorImpl implements ProjectEvaluator {
    public static String EXAM_DIR = "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\main\\resources\\examDir\\";
    public static final String RESULT_TAG = "result";
        public static final String TEST_FILE_NAME = "TeacherTester";
    public static final String TEST_JAVA_FILE = TEST_FILE_NAME + ".java";
    public static final String JUNIT_JAR_PATH = "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\main\\resources\\libraryResources\\junit-4.12.jar";
    public static final String ERROR_ON_COMPILE = "error on compile";

    @Autowired
    CacheService cacheService;

    @Autowired
    ShellService shellService;

    @Override
    public StatefulStringResponseDTO compileProject(TestLessonDTO testLesson, Integer token) throws IOException {
        writeFiles(testLesson, token);


        FileUtils.copyFileToDirectory(new File(EXAM_DIR + token + "\\" + TEST_JAVA_FILE),
                new File(EXAM_DIR + token + "\\" + RESULT_TAG)
        );

        FileUtils.copyFileToDirectory(new File(JUNIT_JAR_PATH),
                new File(EXAM_DIR + token + "\\" + RESULT_TAG)
        );

        try {
            return shellService.runWithJar(JUNIT_JAR_PATH, EXAM_DIR + token + "\\" + RESULT_TAG, TEST_FILE_NAME);
        } catch (Exception e) {
            e.printStackTrace();
            return new StatefulStringResponseDTO(ERROR_ON_COMPILE, false);
        } finally {
            FileUtils.deleteDirectory(new File(EXAM_DIR + token));
        }
    }


    private void writeFiles(TestLessonDTO testLesson, Integer token) {
        new File(EXAM_DIR + token).mkdir();

        int resultDirIndex = testLesson.getTestLesson().getTestFilePath().lastIndexOf("\\");
        String rootProjectDir = testLesson.getTestLesson().getTestFilePath().substring(
                0, resultDirIndex);

        try (Stream<Path> paths = Files.walk(Paths.get(rootProjectDir))) {
            paths.forEach(path -> {
                if (path.toFile().isFile()) {
                    String destPath = EXAM_DIR + token + path.toString().substring(rootProjectDir.length());
                    File destFile = new File(destPath);
                    try {
                        FileUtils.copyFile(path.toFile(), destFile);
                        List<String> cacheCode = cacheService.listMessages(token + path.toString());
                        if (CollectionUtils.isNotEmpty(cacheCode)) {
                            FileUtils.writeStringToFile(destFile, cacheCode.get(0));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }


            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
