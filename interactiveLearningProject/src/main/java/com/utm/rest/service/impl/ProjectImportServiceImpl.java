package com.utm.rest.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.utm.rest.exceptions.TrackNameExistException;
import com.utm.rest.filecrawler.FileCrawlerImpl;
import com.utm.rest.filecrawler.model.FolderElement;
import com.utm.rest.jsonpersistence.JsonInsertion;
import com.utm.rest.model.*;
import com.utm.rest.service.LanguageService;
import com.utm.rest.service.ProjectImportService;
import com.utm.rest.service.TrackService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

@Service("projectImportService")
public class ProjectImportServiceImpl implements ProjectImportService {
    public static final String resourcePpath = "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\main\\resources\\tracksResources";
    public static final String STEP_PREFIX = "step";
    public static final String FOCUS_FILE_PREFIX = "focusFile";
    public static final String INFO_FILE_TXT = "\\info.txt";
    public static final String INFO_PREFIX = "info";
    public static final String TEST_INFO_PREFIX = "testinfo";
    public static final String RESULT_PREFIX = "result";
    public static final String TEACHER_TESTER_PREFIX = "";

    @Autowired
    LanguageService languageService;

    @Autowired
    TrackService trackService;

    @Autowired
    JsonInsertion jsonInsertion;

    @Override
    public TrackContainer scanProjectFiles(String langPath, String containerPath) throws IOException {

        String path = resourcePpath + "\\" + langPath + "\\" + containerPath;

        TrackContainer trackContainer = new TrackContainer();

        File trackRoot = new File(path);
        int i = 0;
        for (File lessonFolder : trackRoot.listFiles()) {
            Lesson lesson = null;
            if (lessonFolder.listFiles()[0].getName().startsWith(INFO_PREFIX)) {
                //infoLesson
                lesson = searchInfoLesson(lessonFolder);
            } else {
                for (File file : lessonFolder.listFiles()) {
                    if (file.isDirectory() && file.getName().startsWith(STEP_PREFIX)) {
                        //projectLesson
                        lesson = searchProjectLesson(lessonFolder);
                    } else if (file.isDirectory() && file.getName().startsWith(RESULT_PREFIX)) {
                        //testLesson
                        lesson = searchTestLesson(lessonFolder, file);
                    }
                }
            }

            if (lesson != null) {
                if (i == 0) {
                    setFirstLessonType(trackContainer, lesson);
                }
                lesson.setIndex(trackContainer.getLessons().size());
                trackContainer.getLessons().add(lesson);
            }
            i++;
        }
        return trackContainer;
    }

    @Override
    public String getValidLanguageName(int langId, String trackName) {
        Language foundLang = languageService.getById(langId);

        if (foundLang != null) {
            List<Track> tracks = trackService.getAllByLanguage(langId);
            for (Track track : tracks) {
                if (track.getName().equalsIgnoreCase(trackName)) {
                    throw new TrackNameExistException(trackName);
                }
            }
            return foundLang.getName();
        }
        return null;
    }

    @Override
    public void persistTrack(TrackContainer trackContainer, int languageId, String trackName) throws JsonProcessingException {
        Track track = new Track();
        track.setLanguage_id(languageId);
        track.setName(trackName);
        track.setFirstLessonType(trackContainer.getFirstLessonType());

        Integer trackId = trackService.persistGetId(track);

        jsonInsertion.insertValueById(trackId, trackContainer);

    }

    private Lesson searchInfoLesson(File lessonFolder) throws IOException {
        Lesson lesson;
        InfoLesson infoLesson = new InfoLesson();
        try (BufferedReader br = new BufferedReader(new FileReader(lessonFolder.listFiles()[0].getAbsolutePath()))) {
            StringBuffer stringBuffer = new StringBuffer();

            String line = br.readLine();
            while (line != null) {
                stringBuffer.append(line);
                line = br.readLine();
            }
            infoLesson.setHtmlText(stringBuffer.toString());
        }
        lesson = infoLesson;
        return lesson;
    }

    private Lesson searchProjectLesson(File lessonFolder) throws IOException {
        Lesson lesson;
        ProjectLesson projectLesson = new ProjectLesson();
        for (File stepFolder : lessonFolder.listFiles()) {
            Step step = scanProjectInfo(stepFolder);
            scanProjectFiles(stepFolder, step);
            FileCrawlerImpl.setFocused(step.getFolderElement(), step.getFocusedFilePath());
            projectLesson.getSteps().add(step);
        }
        lesson = projectLesson;
        return lesson;
    }

    private TestLesson searchTestLesson(File lessonFolder, File projectPath) throws IOException {
        TestLesson testLesson = setTestInfo(lessonFolder);

        FolderElement[] folderElement = new FileCrawlerImpl().getPath(projectPath.getAbsolutePath());
        FileCrawlerImpl.setFocused(folderElement, testLesson.getFocusedFilePath());
        testLesson.setFolderElement(folderElement);

        return testLesson;
    }

    private TestLesson setTestInfo(File lessonFolder) throws IOException {
        TestLesson testLesson = new TestLesson();
        for (File file : lessonFolder.listFiles()) {
            if (file.isFile()) {
                if (file.getName().startsWith(TEST_INFO_PREFIX)) {
                    try (BufferedReader br = new BufferedReader(new FileReader(file.getAbsolutePath()))) {
                        int i = 0;
                        StringBuilder exerciseBuffer = new StringBuilder();
                        String line = br.readLine();
                        while (line != null) {
                            if (i == 0 && line.startsWith(FOCUS_FILE_PREFIX)) {
                                String focusedFilePath = lessonFolder.getAbsolutePath() + "\\" + line.replace(FOCUS_FILE_PREFIX, "").trim();
                                testLesson.setFocusedFilePath(focusedFilePath);
                            } else {
                                exerciseBuffer.append(line);
                            }
                            line = br.readLine();
                            i++;
                        }
                        testLesson.setExercise(exerciseBuffer.toString());
                    }
                } else if (file.getName().startsWith(TEACHER_TESTER_PREFIX)) {
                    testLesson.setTestFilePath(file.getAbsolutePath());
                }
            }

        }
        return testLesson;

    }

    private void setFirstLessonType(TrackContainer trackContainer, Lesson firstLesson) {
        if (CollectionUtils.isEmpty(trackContainer.getLessons())
                && firstLesson != null
                && firstLesson.getLessonType() != null) {
            trackContainer.setFirstLessonType(firstLesson.getLessonType());
        }
    }

    private void scanProjectFiles(File stepFolder, Step step) {
        for (File projectFolder : stepFolder.listFiles()) {
            if (projectFolder.isDirectory()) {
                FolderElement[] folderElement = new FileCrawlerImpl().getPath(projectFolder.getAbsolutePath());
                FileCrawlerImpl.setFocused(folderElement, step.getFocusedFilePath());
                step.setFolderElement(folderElement);
                break;
            }
        }
    }

    private Step scanProjectInfo(File stepFolder) throws IOException {
        Step step = new Step();
        try (BufferedReader br = new BufferedReader(new FileReader(stepFolder.getAbsolutePath() + INFO_FILE_TXT))) {
            int i = 0;
            String line = br.readLine();
            while (line != null) {
                if (i == 0 && line.startsWith(FOCUS_FILE_PREFIX)) {
                    String focusedFilePath = stepFolder.getAbsolutePath() + "\\" + line.replace(FOCUS_FILE_PREFIX, "").trim();
                    step.setFocusedFilePath(focusedFilePath);
                } else {
                    StepLine stepLine = new StepLine();
                    stepLine.setId(i - 1);
                    stepLine.setHtmlText(line.replaceAll("^[0-9]*", "").trim()
                            .replaceAll("^[-]", "").trim());
                    step.getStepLines().add(stepLine);

                }
                line = br.readLine();
                i++;
            }
        }
        return step;
    }
}
