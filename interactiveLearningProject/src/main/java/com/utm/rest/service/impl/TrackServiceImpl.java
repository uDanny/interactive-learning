package com.utm.rest.service.impl;

import com.utm.rest.dao.TrackDao;
import com.utm.rest.model.Track;
import com.utm.rest.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("trackService")
@Transactional
public class TrackServiceImpl implements TrackService {

    @Autowired
    TrackDao trackDao;

    @Override
    public List<Track> getAllByLanguage(int languageId) {
        List<Track> tracks = trackDao.getAllByLanguageID(languageId);
        return tracks;
    }

    @Override
    public Integer persistGetId(Track track) {
        return trackDao.persistGetId(track);
    }
}
