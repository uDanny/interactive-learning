package com.utm.rest.dto.impl;

import com.utm.rest.enums.LessonType;
import com.utm.rest.model.TestLesson;

public class TestLessonDTO {
    private int token;
    private int lessonIndex;
    private int lessonCount;
    private int trackId;
    private LessonType nextLessonType;
    private String initialCode;

    private TestLesson testLesson;

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public int getLessonIndex() {
        return lessonIndex;
    }

    public void setLessonIndex(int lessonIndex) {
        this.lessonIndex = lessonIndex;
    }

    public int getLessonCount() {
        return lessonCount;
    }

    public void setLessonCount(int lessonCount) {
        this.lessonCount = lessonCount;
    }

    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    public LessonType getNextLessonType() {
        return nextLessonType;
    }

    public void setNextLessonType(LessonType nextLessonType) {
        this.nextLessonType = nextLessonType;
    }

    public TestLesson getTestLesson() {
        return testLesson;
    }

    public void setTestLesson(TestLesson testLesson) {
        this.testLesson = testLesson;
    }

    public String getInitialCode() {
        return initialCode;
    }

    public void setInitialCode(String initialCode) {
        this.initialCode = initialCode;
    }
}
