package com.utm.rest.dto.impl;

import com.utm.rest.enums.LessonType;

public class InfoLessonDTO {
    private int lessonIndex;
    private int lessonCount;
    private int trackId;
    private LessonType nextLessonType;
    private String htmlText;

    public int getLessonIndex() {
        return lessonIndex;
    }

    public void setLessonIndex(int lessonIndex) {
        this.lessonIndex = lessonIndex;
    }

    public int getLessonCount() {
        return lessonCount;
    }

    public void setLessonCount(int lessonCount) {
        this.lessonCount = lessonCount;
    }

    public LessonType getNextLessonType() {
        return nextLessonType;
    }

    public void setNextLessonType(LessonType nextLessonType) {
        this.nextLessonType = nextLessonType;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }
}
