package com.utm.rest.dto.impl;

import java.util.Objects;

public class StatefulStringResponseDTO extends StringResponseDTO {
    private boolean accepted;

    public StatefulStringResponseDTO(String response) {
        super(response);
    }

    public StatefulStringResponseDTO(String response, boolean accepted) {
        super(response);
        this.accepted = accepted;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        StatefulStringResponseDTO that = (StatefulStringResponseDTO) o;
        return accepted == that.accepted;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), accepted);
    }
}
