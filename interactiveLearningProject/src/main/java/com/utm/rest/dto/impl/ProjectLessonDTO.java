package com.utm.rest.dto.impl;

import com.utm.rest.enums.LessonType;
import com.utm.rest.model.Step;

public class ProjectLessonDTO {
    private int lessonIndex;
    private int lessonCount;
    private int stepIndex;
    private int stepCount;
    private int trackId;
    private LessonType nextLessonType;
    private String initialCode;
    private Step step;

    public int getLessonIndex() {
        return lessonIndex;
    }

    public void setLessonIndex(int lessonIndex) {
        this.lessonIndex = lessonIndex;
    }

    public int getLessonCount() {
        return lessonCount;
    }

    public void setLessonCount(int lessonCount) {
        this.lessonCount = lessonCount;
    }

    public int getStepIndex() {
        return stepIndex;
    }

    public void setStepIndex(int stepIndex) {
        this.stepIndex = stepIndex;
    }

    public int getStepCount() {
        return stepCount;
    }

    public void setStepCount(int stepCount) {
        this.stepCount = stepCount;
    }

    public LessonType getNextLessonType() {
        return nextLessonType;
    }

    public void setNextLessonType(LessonType nextLessonType) {
        this.nextLessonType = nextLessonType;
    }

    public Step getStep() {
        return step;
    }

    public void setStep(Step step) {
        this.step = step;
    }

    public String getInitialCode() {
        return initialCode;
    }

    public void setInitialCode(String initialCode) {
        this.initialCode = initialCode;
    }

    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }
}
