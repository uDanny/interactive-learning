package com.utm.rest.dto.impl;

import java.util.Objects;

public class StringResponseDTO {
    String response;

    public StringResponseDTO() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringResponseDTO that = (StringResponseDTO) o;
        return Objects.equals(response, that.response);
    }

    @Override
    public int hashCode() {

        return Objects.hash(response);
    }

    public StringResponseDTO(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
