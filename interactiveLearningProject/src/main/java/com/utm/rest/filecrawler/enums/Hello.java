package com.utm.rest.filecrawler.enums;

public class Hello {
    public static void main(String[] args) {
        String staticMessage = HelloResources.HELLO_WORLD_TEXT;

        HelloResources helloResources = new HelloResources();
        helloResources.showMessage(staticMessage);
    }
}
