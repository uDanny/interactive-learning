package com.utm.rest.filecrawler.model;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.utm.rest.filecrawler.enums.FileType;

@JsonTypeName("fileElement")
public class FileElement extends PathElement {
    public FileElement() {
        fileType = FileType.FILE;
    }
}
