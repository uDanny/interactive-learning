package com.utm.rest.filecrawler.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.utm.rest.filecrawler.enums.FileType;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonTypeName("folderElement")
public class FolderElement extends PathElement {

    @JsonProperty(value = "children")
    private List<PathElement> supPaths = new ArrayList<>();

    public FolderElement() {
        fileType = FileType.FOLDER;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FolderElement that = (FolderElement) o;
        return Objects.equals(supPaths, that.supPaths);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), supPaths);
    }

    public List<PathElement> getSupPaths() {
        return supPaths;
    }

    public void setSupPaths(List<PathElement> supPaths) {
        this.supPaths = supPaths;
    }
}
