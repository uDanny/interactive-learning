package com.utm.rest.filecrawler.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.utm.rest.filecrawler.enums.FileType;

import java.util.Objects;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({

        @JsonSubTypes.Type(value = FolderElement.class, name = "folderElement"),

        @JsonSubTypes.Type(value = FileElement.class, name = "fileElement")

})
public abstract class PathElement {
    @JsonProperty
    protected String path;
    @JsonProperty("roleName")
    protected String name;
    @JsonProperty
    protected FileType fileType;
    @JsonProperty
    protected boolean newElement = true;
    @JsonProperty
    protected boolean focused = false;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PathElement that = (PathElement) o;
        return newElement == that.newElement &&
                focused == that.focused &&
                Objects.equals(path, that.path) &&
                Objects.equals(name, that.name) &&
                fileType == that.fileType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(path, name, fileType, newElement, focused);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public boolean isNewElement() {
        return newElement;
    }

    public void setNewElement(boolean newElement) {
        this.newElement = newElement;
    }

    public boolean isFocused() {
        return focused;
    }

    public void setFocused(boolean focused) {
        this.focused = focused;
    }

}
