package com.utm.rest.filecrawler.enums;

public class HelloResources {
    public static final String HELLO_WORLD_TEXT = "Hello from Resources!";
    public void showMessage(String message){
        System.out.println(message);
    }
}
