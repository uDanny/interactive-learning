package com.utm.rest.filecrawler;

import com.utm.rest.filecrawler.model.FileElement;
import com.utm.rest.filecrawler.model.FolderElement;
import com.utm.rest.filecrawler.model.PathElement;

import java.io.File;

public class FileCrawlerImpl {

    public FolderElement walk(String path) {

        FolderElement parentFolderElement = new FolderElement();
        parentFolderElement.setName("root");
        parentFolderElement.setPath(path);

        eachPathElement(parentFolderElement);

        System.out.println(parentFolderElement);
        return parentFolderElement;
    }

    public FolderElement[] getPath(String path) {
        FileCrawlerImpl fw = new FileCrawlerImpl();
        FolderElement folderElement = fw.walk(path);
        FolderElement[] folderElements = {folderElement};
        return folderElements;
    }

    public static void setFocused(FolderElement[] folderElements, String focusedPath) {
        for (FolderElement folderElement : folderElements) {
            searchFocused(folderElement, focusedPath);
        }
    }

    private void eachPathElement(FolderElement folderElement) {
        File root = new File(folderElement.getPath());
        File[] list = root.listFiles();
        if (list != null) {
            for (File f : list) {


                if (f.isDirectory()) {
                    FolderElement newFolderElement = new FolderElement();
                    newFolderElement.setName(f.getName());
                    newFolderElement.setPath(f.getAbsolutePath());

                    folderElement.getSupPaths().add(newFolderElement);

                    eachPathElement(newFolderElement);

                } else {
                    FileElement newFile = new FileElement();
                    newFile.setName(f.getName());
                    newFile.setPath(f.getAbsolutePath());

                    folderElement.getSupPaths().add(newFile);
                }
            }
        }
    }

    private static void searchFocused(FolderElement folderElement, String focusedPath) {
        for (PathElement eachElement : folderElement.getSupPaths()) {
            if (eachElement instanceof FolderElement) {
                searchFocused((FolderElement) eachElement, focusedPath);

            } else if (eachElement instanceof FileElement) {
                if (eachElement.getPath().equals(focusedPath)) {
                    eachElement.setFocused(true);
                    break;
                }
            }
        }
    }
}
