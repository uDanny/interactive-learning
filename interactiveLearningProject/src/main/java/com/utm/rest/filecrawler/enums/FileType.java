package com.utm.rest.filecrawler.enums;

public enum FileType {
    FILE("File"),
    FOLDER("Folder");

    String type;

    FileType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
