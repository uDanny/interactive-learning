package com.utm.rest.enums;

public enum LessonType {

    INFO("Info"),
    PROJECT("Project"),
    TEST("Test");

    String type;

    LessonType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
