package com.utm.rest;

import com.utm.rest.config.SpringDBConfig;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringDBConfig.class, loader = AnnotationConfigContextLoader.class)
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Transactional
public abstract class AbstractDBTest {
    @PersistenceContext
    protected EntityManager entityManager;
}