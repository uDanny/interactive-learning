package com.utm.rest.dao;

import com.utm.rest.AbstractDBTest;
import com.utm.rest.enums.LessonType;
import com.utm.rest.model.Track;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import javax.persistence.Query;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:languageData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:trackData.sql")
})
public class TrackDaoTestDB extends AbstractDBTest {
    @Autowired
    TrackDao trackDao;

    @Test
    public void getAllTest() {
        List<Track> all = trackDao.getAll();
        assertEquals(all.get(0).getName(), "firstTrack");
    }

    @Test
    public void getByIdTest() {
        List<Track> allByLanguageID = trackDao.getAllByLanguageID(1);
        assertEquals(allByLanguageID.get(0).getName(), "firstTrack");
    }

    @Test
    public void persistGetIdTest() {
        Track track = new Track();
        track.setName("trackSomeName");
        track.setFirstLessonType(LessonType.INFO);
        Integer generatedId = trackDao.persistGetId(track);
        assertNotNull(generatedId);


        Query q = entityManager.createQuery(
                "SELECT e FROM " + Track.class.getName() + " e");
        List resultList = q.getResultList();

        assertEquals(2, resultList.size());
    }
}