package com.utm.rest.dao;

import com.utm.rest.AbstractDBTest;
import com.utm.rest.model.Language;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static org.junit.Assert.assertEquals;


@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:languageData.sql")
public class LanguageDaoTestDB extends AbstractDBTest {
    @Autowired
    LanguageDao languageDao;

    @Test
    public void getAllTest() {
        List<Language> actualLang = languageDao.getAll();

        assertEquals(actualLang.get(0).getName(), "firstLang");
    }

    @Test
    public void getByIdTest() {
        Language actualLang = languageDao.getById(1);
        assertEquals(actualLang.getName(), "firstLang");

    }

}