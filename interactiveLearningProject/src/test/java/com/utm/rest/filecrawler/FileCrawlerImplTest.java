package com.utm.rest.filecrawler;

import com.utm.rest.filecrawler.enums.FileType;
import com.utm.rest.filecrawler.model.FileElement;
import com.utm.rest.filecrawler.model.FolderElement;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FileCrawlerImplTest {

    private static final String PATH = "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\test\\resource\\tracksResources\\Java\\track6\\les0\\step0";

    @Test
    void fileWalk() {
        FileCrawlerImpl fw = new FileCrawlerImpl();
        FolderElement folderElement = fw.walk(PATH);

        assertEquals(getExpectedFolderElement(), folderElement);
    }

    @Test
    void setFocused() {
        FileCrawlerImpl fw = new FileCrawlerImpl();
        FolderElement actualFolderElement = getExpectedFolderElement();
        actualFolderElement.getSupPaths().get(1).setFocused(true);

        FolderElement expectedFolderElement = getExpectedFolderElement();
        fw.setFocused(new FolderElement[]{expectedFolderElement}, "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\test\\resource\\tracksResources\\Java\\track6\\les0\\step0\\info.txt");

        assertEquals(expectedFolderElement, actualFolderElement);
    }

    private FolderElement getExpectedFolderElement() {
        FolderElement expectedFolderElement = new FolderElement();
        expectedFolderElement.setFocused(false);
        expectedFolderElement.setNewElement(true);
        expectedFolderElement.setFileType(FileType.FOLDER);
        expectedFolderElement.setName("root");
        expectedFolderElement.setPath("D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\test\\resource\\tracksResources\\Java\\track6\\les0\\step0");

        expectedFolderElement.setSupPaths(new ArrayList<>(Arrays.asList(getInnerFolder(), getInerFile())));
        return expectedFolderElement;
    }

    private FileElement getInerFile() {
        FileElement innerFile = new FileElement();
        innerFile.setPath("D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\test\\resource\\tracksResources\\Java\\track6\\les0\\step0\\info.txt");
        innerFile.setName("info.txt");
        innerFile.setFileType(FileType.FILE);
        innerFile.setNewElement(true);
        innerFile.setFocused(false);
        return innerFile;
    }

    private FolderElement getInnerFolder() {
        FolderElement innerFolder = new FolderElement();
        innerFolder.setFileType(FileType.FOLDER);
        innerFolder.setNewElement(true);
        innerFolder.setFocused(false);
        innerFolder.setName("a");
        innerFolder.setPath("D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\test\\resource\\tracksResources\\Java\\track6\\les0\\step0\\a");
        innerFolder.setSupPaths(new ArrayList<>(Collections.singleton(getInnerFolderFile())));
        return innerFolder;
    }

    private FileElement getInnerFolderFile() {
        FileElement innerFolderFile = new FileElement();
        innerFolderFile.setPath("D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\test\\resource\\tracksResources\\Java\\track6\\les0\\step0\\a\\Hello.txt");
        innerFolderFile.setName("Hello.txt");
        innerFolderFile.setFileType(FileType.FILE);
        innerFolderFile.setNewElement(true);
        innerFolderFile.setFocused(false);
        return innerFolderFile;
    }
}