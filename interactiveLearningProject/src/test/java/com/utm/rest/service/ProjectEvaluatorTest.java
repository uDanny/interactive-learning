package com.utm.rest.service;

import com.utm.rest.dto.impl.StatefulStringResponseDTO;
import com.utm.rest.dto.impl.TestLessonDTO;
import com.utm.rest.model.TestLesson;
import com.utm.rest.redis.CacheService;
import com.utm.rest.service.impl.ProjectEvaluatorImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class ProjectEvaluatorTest {
    @Mock
    CacheService cacheService;

    @Mock
    ShellService shellService;

    @InjectMocks
    @Spy
    ProjectEvaluator projectEvaluator = new ProjectEvaluatorImpl();

    private static final String RESPONSE = "response";
    private static final Integer TOKEN = 123;
    private static final String PATH = "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\test\\resource";
    private static final String TEST_EXAM_DIR = PATH + "\\" + "examDir\\";
    private static final String JUNIT_JAR_PATH = "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\main\\resources\\libraryResources\\junit-4.12.jar";
    private static final String RESULT_TAG = "result";
    private static final String TEST_FILE_NAME = "TeacherTester";

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(cacheService.listMessages(anyString())).thenReturn(new ArrayList<>());
        when(shellService.runWithJar(anyString(), anyString(), anyString())).thenReturn(new StatefulStringResponseDTO(RESPONSE, true));

        Field field = ReflectionUtils.findField(ProjectEvaluatorImpl.class, "EXAM_DIR");
        ReflectionUtils.setField(field, projectEvaluator, TEST_EXAM_DIR);

    }

    @Test
    void testCompileProject() throws Exception {
        TestLesson testLesson = new TestLesson();
        testLesson.setTestFilePath(PATH + "\\tracksResources\\Java\\track6\\les2\\TeacherTester.java");

        TestLessonDTO testLessonDTO = new TestLessonDTO();
        testLessonDTO.setTestLesson(testLesson);
        StatefulStringResponseDTO responseDTO = projectEvaluator.compileProject(testLessonDTO, TOKEN);

        verify(shellService, times(1)).runWithJar(JUNIT_JAR_PATH, TEST_EXAM_DIR + TOKEN + "\\" + RESULT_TAG, TEST_FILE_NAME);
    }

}