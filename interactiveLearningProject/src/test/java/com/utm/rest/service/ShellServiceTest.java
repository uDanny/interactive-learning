package com.utm.rest.service;

import com.utm.rest.AbstractTest;
import com.utm.rest.dto.impl.StatefulStringResponseDTO;
import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

import static junit.framework.TestCase.assertEquals;

public class ShellServiceTest extends AbstractTest {
    private static final String EXAM_DIR = "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\test\\resource\\shellRunDir";
    private static final String TEST_FILE_NAME = "TeacherTester";
    private static final String JUNIT_JAR_PATH = "D:\\IT\\Teza\\interactive-learning\\interactiveLearningProject\\src\\main\\resources\\libraryResources\\junit-4.12.jar";
    public static final String RESPONSE_CODE = "execution code:  exitValue() 0\n" +
            "run code   \n" +
            " out: right one \n" +
            " err: Exception in thread \"main\" org.junit.ComparisonFailure: expected:<right one[]> but was:<right one[2]> \n" +
            " err: \tat org.junit.Assert.assertEquals(Assert.java:115) \n" +
            " err: \tat org.junit.Assert.assertEquals(Assert.java:144) \n" +
            " err: \tat TeacherTester.main(TeacherTester.java:7) exitValue() 1";

    @Autowired
    ShellService shellService;

    @After
    public void tearDown() {
        File root = new File(EXAM_DIR);
        Arrays.stream(Objects.requireNonNull(root.listFiles((f, p) -> p.endsWith(".class")))).forEach(File::delete);
    }

    @Test
    public void name() throws Exception {
        StatefulStringResponseDTO responseDTO = shellService.runWithJar(JUNIT_JAR_PATH, EXAM_DIR, TEST_FILE_NAME);
        StatefulStringResponseDTO expectedResponseDTO = new StatefulStringResponseDTO(RESPONSE_CODE, false);

        assertEquals(expectedResponseDTO, responseDTO);

    }
}