# Spring full-stack project
Project built on Java 8, Spring Boot, JPA/Hibernate, Postgres, Redis, Mockito, H2 in-memory DB as a back-end and AngularJS with ACE editor library for the front-end.